package ru.sctbelpa.productomer_r4_2;

import org.junit.Test;

import java.util.Random;

import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

import static org.junit.Assert.*;

// Эти тесты запускать на этапе написания кода
public class ProtocolTest {
    @Test
    public void create_messageObjects() throws Exception {
        System.out.println("create_messageObjects");
        ProtobufDevice0000E002.Request request =
                ProtobufDevice0000E002.Request.getDefaultInstance();
        ProtobufDevice0000E002.Response response =
                ProtobufDevice0000E002.Response.getDefaultInstance();
    }

    @Test
    public void test_pingRequest() throws Exception {
        System.out.println("test_pingRequest");
        final Random id = new Random();

        ProtobufDevice0000E002.Request request = ProtobufDevice0000E002.Request.newBuilder()
                .setId(id.nextInt())
                .setDeviceID(ProtobufDevice0000E002.INFO.ID_DISCOVER.getNumber())
                .setProtocolVersion(0)
                .build();
        byte [] result = request.toByteArray();

        ProtobufDevice0000E002.Request res = ProtobufDevice0000E002.Request.parseFrom(result);

        assertEquals(request.getId(), res.getId());
        assertEquals(request.getDeviceID(), res.getDeviceID());
        assertEquals(request.getProtocolVersion(), res.getProtocolVersion());
    }
}