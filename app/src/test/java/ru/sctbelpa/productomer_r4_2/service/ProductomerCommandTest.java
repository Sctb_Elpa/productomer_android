package ru.sctbelpa.productomer_r4_2.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductomerCommandTest {
    @Mock
    Dispatcher dispatcher;

    @Before
    public void startup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSetRequest() throws Exception {
        ProductomerCommand cmd = new ProductomerCommand(dispatcher);
        assertEquals(cmd.getState(), IProductomerCommand.STATE.UNINITIALISED);
        cmd.setRequest(ProtobufDevice0000E002.Request.getDefaultInstance());
        assertEquals(cmd.getState(), IProductomerCommand.STATE.INITIALISED);
    }

    @Test(expected = IllegalStateException.class)
    public void testExecuteUninitialised() throws Exception {
        ProductomerCommand cmd = new ProductomerCommand(dispatcher);
        cmd.execute();
    }

    @Test(expected = IllegalStateException.class)
    public void testSetRequestAfterExecute() throws Exception {
        ProtobufDevice0000E002.Request request =
                ProtobufDevice0000E002.Request.getDefaultInstance();
        ProductomerCommand cmd = new ProductomerCommand(dispatcher, request);
        when(dispatcher.isRunning()).thenReturn(true);
        cmd.execute();
        assertEquals(cmd.getState(), IProductomerCommand.STATE.SENT);
        cmd.setRequest(request);
    }

    @Test
    public void testDispatcherNotReady() throws Exception {
        ProtobufDevice0000E002.Request request =
                ProtobufDevice0000E002.Request.getDefaultInstance();
        ProductomerCommand cmd = new ProductomerCommand(dispatcher, request);
        IFailCallback fcb = mock(IFailCallback.class);
        cmd.onError(fcb);

        cmd.execute();

        verify(fcb).processError(any(InterruptedException.class));
        assertEquals(IProductomerCommand.STATE.ERROR, cmd.getState());
    }

    @Test
    public void testExecute() throws Exception {
        ProtobufDevice0000E002.Request request =
                ProtobufDevice0000E002.Request.getDefaultInstance();
        ProductomerCommand cmd = new ProductomerCommand(dispatcher, request);
        when(dispatcher.isRunning()).thenReturn(true);
        cmd.execute();

        verify(dispatcher).executeCommand(cmd);
        assertEquals(cmd.getState(), IProductomerCommand.STATE.SENT);
    }

    @Test
    public void testCallbacks() throws Exception {
        ProductomerCommand cmd = new ProductomerCommand(dispatcher);
        ISuccessCallback scb = mock(ISuccessCallback.class);
        IFailCallback fcb = mock(IFailCallback.class);
        cmd
                .onSuccess(scb)
                .onError(fcb);

        ProtobufDevice0000E002.Response resp = ProtobufDevice0000E002.Response.getDefaultInstance();
        cmd.Success(resp);
        verify(scb).processResult(resp);

        Exception e = new RuntimeException();
        cmd.Fail(e);
        verify(fcb).processError(e);
    }

    @Test(expected = NullPointerException.class)
    public void testNullRequestId() throws Exception {
        IProductomerCommand cmd = new ProductomerCommand(dispatcher);
        cmd.getId();
    }

    @Test
    public void testRequestId() throws Exception {
        ProtobufDevice0000E002.Request req = ProtobufDevice0000E002.Request.newBuilder()
                .setId(42)
                .setProtocolVersion(0)
                .setDeviceID(ProtobufDevice0000E002.INFO.ID_DISCOVER.getNumber())
                .build()
                ;

        IProductomerCommand cmd = new ProductomerCommand(dispatcher, req);

        assertEquals(42, cmd.getId());
    }

    @Test
    public void testCancelUnusedRequest() throws Exception {
        IProductomerCommand cmd = new ProductomerCommand(dispatcher);
        assertFalse(cmd.cancel());
        assertEquals(IProductomerCommand.STATE.UNINITIALISED, cmd.getState());
        cmd.setRequest(ProtobufDevice0000E002.Request.getDefaultInstance());
        assertFalse(cmd.cancel());
        assertEquals(IProductomerCommand.STATE.INITIALISED, cmd.getState());
    }

    @Test
    public void testCancelRequest() throws Exception {
        ProtobufDevice0000E002.Request req = ProtobufDevice0000E002.Request.getDefaultInstance();

        IProductomerCommand cmd = new ProductomerCommand(dispatcher, req);

        when(dispatcher.removeCommandFromPendingSet(
                any(IProductomerCommand.class))).thenReturn(true);
        when(dispatcher.isRunning()).thenReturn(true);

        cmd.execute();
        assertEquals(IProductomerCommand.STATE.SENT, cmd.getState());
        cmd.cancel();
        assertEquals(IProductomerCommand.STATE.ERROR, cmd.getState());
        verify(dispatcher).removeCommandFromPendingSet(cmd);
    }
}
