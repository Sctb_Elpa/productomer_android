package ru.sctbelpa.productomer_r4_2.service;


import org.junit.Test;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeoutException;

import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class InteractCommandWithDispatcher {

    @Test
    public void testTimeout() throws Exception {
        ProtobufDevice0000E002.Request req = ProtobufDevice0000E002.Request.getDefaultInstance();

        Dispatcher dispatcher =
                spy(new Dispatcher(mock(InputStream.class), mock(OutputStream.class)));
        IProductomerCommand cmd = new ProductomerCommand(dispatcher, req);
        IFailCallback ecb = mock(IFailCallback.class);

        cmd
                .setTimeout(10)
                .onError(ecb);

        assertTrue(dispatcher.getPendingCommands().isEmpty());

        dispatcher.start();
        cmd.execute();

        Thread.sleep(20);
        verify(ecb).processError(any(TimeoutException.class));
        verify(dispatcher).removeCommandFromPendingSet(cmd);
        assertTrue(dispatcher.getPendingCommands().isEmpty());
    }
}
