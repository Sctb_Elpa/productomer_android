package ru.sctbelpa.productomer_r4_2.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.UUID;

import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

public class ProductomerService extends Service {
    static final String LOG_TAG = "ProductomerService";
    public static final UUID defaultUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    BluetoothAdapter adapter;
    BluetoothDevice device;
    UUID targetUUID;
    BluetoothSocket bt_socket;
    Dispatcher dispatcher = null;

    class CommandBuilder {
        // may use one builder for create many messages

        private final ProductomerService productomerService;
        private final ProtobufDevice0000E002.Request.Builder requestBuilder;

        public ISuccessCallback successCallback = null;
        public IFailCallback failCallback = null;

        CommandBuilder(ProductomerService service) {
            productomerService = service;
            requestBuilder = CreateDefaultCommandBuilder();
        }

        public CommandBuilder settingsOperation(@Nullable ProtobufDevice0000E002.WriteSettingsRequest newSettings) {
            if (newSettings != null) {
                requestBuilder.setWriteSettings(newSettings);
            } else {
                requestBuilder.setWriteSettings(
                        ProtobufDevice0000E002.WriteSettingsRequest.getDefaultInstance());
            }
            return this;
        }

        public CommandBuilder settingsOperation(ProtobufDevice0000E002.WriteSettingsRequest.Builder newSettingsBuilder) {
            if (newSettingsBuilder != null) {
                requestBuilder.setWriteSettings(newSettingsBuilder);
            } else {
                requestBuilder.setWriteSettings(
                        ProtobufDevice0000E002.WriteSettingsRequest.getDefaultInstance());
            }
            return this;
        }

        public IProductomerCommand build(int targetDeviceID) throws IOException {
            productomerService.ensureConnected();
            requestBuilder.setId(dispatcher.genNewUnusedId()).setDeviceID(targetDeviceID);
            return new ProductomerCommand(productomerService.dispatcher, requestBuilder.build())
                    .onSuccess(successCallback).onError(failCallback);
        }

        public IProductomerCommand buildBroadcast() throws IOException {
            return build(0);
        }

        private ProtobufDevice0000E002.Request.Builder CreateDefaultCommandBuilder() {
            return ProtobufDevice0000E002.Request.newBuilder()
                    .setProtocolVersion(ProtobufDevice0000E002.INFO.PROTOCOL_VERSION_VALUE);
        }
    }

    @Override
    public void onCreate() throws NullPointerException {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
        adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null)
            throw new NullPointerException("No bluetooth Adapters present in system");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return new LocalBinder();
    }

    @Contract(pure = true)
    public final BluetoothAdapter getAdapter() {
        return adapter;
    }

    public void setTargetServer(BluetoothDevice device, UUID uuid) {
        if (bt_socket != null && bt_socket.isConnected()) {
            disconnect();
        }
        if (device == null)
            throw new NullPointerException("Device == null");

        this.device = device;
        this.targetUUID = uuid == null ? defaultUUID : uuid;
    }

    private void disconnect() {
        if (bt_socket != null) {
            try {
                bt_socket.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, String.format("IOException while closing BluetoothSocket (%s)",
                        e.getMessage()));
                stopDispatcher();
            }
        }
        targetUUID = null;
        device = null;
        bt_socket = null;
    }

    public void connect() throws IOException {
        adapter.cancelDiscovery();
        try {
            bt_socket = device.createRfcommSocketToServiceRecord(targetUUID);
            bt_socket.connect();
            stopDispatcher();
            startDispatcher();
        } catch (IOException e) {
            Log.e(LOG_TAG, String.format("Can't open connection to %s (%s)",
                    device.toString(), e.getMessage()));
            throw e;
        }
    }

    private void stopDispatcher() {
        if (dispatcher != null)
            try {
                dispatcher.interrupt();
                dispatcher.join();
            } catch (InterruptedException e) { /* no problem there */ }
        dispatcher = null;
    }

    private void startDispatcher() throws IOException {
        dispatcher = new Dispatcher(bt_socket.getInputStream(), bt_socket.getOutputStream());
        dispatcher.start();
    }

    private void ensureConnected() throws IOException {
        if (bt_socket == null || !bt_socket.isConnected() ||
                dispatcher == null || !dispatcher.isRunning())
            connect();
    }

    @Contract(pure = true)
    public boolean isConnected() {
        return (bt_socket != null) && (bt_socket.isConnected());
    }

    @Contract(pure = true)
    public BluetoothDevice getDevice() {
        return device;
    }

    @Contract(pure = true)
    public UUID getUUID() {
        return targetUUID == null ? defaultUUID : targetUUID;
    }

    public CommandBuilder newCommandBuilder() {
        return new CommandBuilder(this);
    }

    public class LocalBinder extends Binder {
        public ProductomerService getService() {
            return ProductomerService.this;
        }
    }
}
