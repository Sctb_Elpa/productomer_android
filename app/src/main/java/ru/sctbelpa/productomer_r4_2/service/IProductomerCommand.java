package ru.sctbelpa.productomer_r4_2.service;


import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

public interface IProductomerCommand {
    enum STATE {
        UNINITIALISED,
        INITIALISED,
        SENT,
        ERROR,
        DONE
    }

    STATE getState();
    IProductomerCommand setRequest(ProtobufDevice0000E002.Request request) throws IllegalStateException;
    IProductomerCommand onSuccess(ISuccessCallback cb);
    IProductomerCommand onError(IFailCallback cb);

    IProductomerCommand setTimeout(long timeout) throws IllegalStateException;
    IProductomerCommand execute() throws Exception;

    boolean cancel();
    int getId() throws NullPointerException;

    long getTimeout();
}
