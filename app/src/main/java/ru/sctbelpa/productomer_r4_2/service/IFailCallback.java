package ru.sctbelpa.productomer_r4_2.service;


public interface IFailCallback {
    void processError(Exception ex);
}
