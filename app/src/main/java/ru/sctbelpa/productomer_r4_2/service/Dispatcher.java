package ru.sctbelpa.productomer_r4_2.service;

import android.support.annotation.Nullable;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import ru.sctbelpa.MagickDelimitedProtobuf.MagickDelimitedProtobufOutputStream;
import ru.sctbelpa.MagickDelimitedProtobuf.MagickDelimitedProtobufReader;
import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

public class Dispatcher extends Thread {
    private static final String LOG_TAG = "CommandDispatcher";

    /// Commands, what now in progress
    private final Map<Integer, ProductomerCommand> pendingCommands;
    @NotNull
    private final InputStream input;
    @NotNull
    private final OutputStream output;

    private MagickDelimitedProtobufReader reader;
    private MagickDelimitedProtobufOutputStream outputStream;

    public Dispatcher(@NotNull InputStream input, @NotNull OutputStream output) {
        this.input = input;
        this.output = output;
        reader = new MagickDelimitedProtobufReader(input);
        outputStream = new MagickDelimitedProtobufOutputStream(output);
        pendingCommands = new HashMap<>();
    }

    synchronized public Void executeCommand(ProductomerCommand command) {
        try {
            outputStream.write(command.getRequest());
        } catch (IOException e) {
            command.Fail(e);
            return null;
        }

        pendingCommands.put(command.getId(), command);
        return null;
    }

    public boolean isRunning() { // make not final
        return isAlive();
    }

    public Collection<? extends IProductomerCommand> getPendingCommands() {
        return pendingCommands.values();
    }

    public boolean removeCommandFromPendingSet(IProductomerCommand cmd) {
        synchronized (pendingCommands) {
            return pendingCommands.remove(cmd.getId()) != null;
        }
    }

    public boolean isCommandPending(IProductomerCommand cmd) {
        return pendingCommands.containsValue(cmd);
    }

    @Override
    public void interrupt() {
        try {
            input.close();
            output.close();
        } catch (IOException e) {
        }
        super.interrupt();
    }

    @Override
    public void run() {
        while (true) {
            ProtobufDevice0000E002.Response response;
            try {
                response = reader.read(ProtobufDevice0000E002.Response.class);
            } catch (InterruptedException ex) {
                Log.w(LOG_TAG, "Reader thread was closed");
                cancelAllPendingCommands();
                return;
            } catch (InterruptedIOException ex) {
                Log.w(LOG_TAG, "Reader was closed while IO operation");
                cancelAllPendingCommands();
                return;
            } catch (MagickDelimitedProtobufReader.EndOfStreamException ex) {
                Log.w(LOG_TAG, "input stream eas closed");
                cancelAllPendingCommands();
                return;
            } catch (Exception ex) {
                ex.printStackTrace();
                continue; /* unknown error */
            }

            ProductomerCommand pendingCommand = findPendingCommandById(response.getId());
            if (pendingCommand != null) {
                pendingCommand.Success(response);
            } else {
                Log.w(LOG_TAG, "Received correct message, bun not pending");
            }
        }
    }

    private void cancelAllPendingCommands() {
        for (IProductomerCommand cmd : pendingCommands.values())
            cmd.cancel();
    }

    @Nullable
    private ProductomerCommand findPendingCommandById(int id) {
        synchronized (pendingCommands) {
            return pendingCommands.containsKey(id) ? pendingCommands.get(id) : null;
        }
    }

    public int genNewUnusedId() {
        Set<Integer> usedIDs = pendingCommands.keySet();
        Random random = new Random();
        while (true) {
            int rndID = random.nextInt();
            if (!usedIDs.contains(rndID))
                return rndID;
        }
    }
}
