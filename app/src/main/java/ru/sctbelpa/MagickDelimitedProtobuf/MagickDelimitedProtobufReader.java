package ru.sctbelpa.MagickDelimitedProtobuf;


import com.google.protobuf.CodedInputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class MagickDelimitedProtobufReader {
    public static final int MAX_MSG_SIZE = 1500;

    // protobuf-dependent names
    public static final String PARSE_FROM_METHOD_NAME = "parseFrom";
    public static final String GET_DEFAULT_INSTANCE = "getDefaultInstance";
    public static final String TO_BYTE_ARRAY = "toByteArray";
    public static final String BIT_FIELD_0_ = "bitField0_";

    private enum STATE {
        RECEIVING_SIZE,
        CHECK_MESSAGE_FIRST_TAG,
        RECEIVING_BODY
    }

    private enum EXCEPTION_CODE {
        MISSING_REQ_FIELDS, INPUT_ENDED_UNEXPECTLY,
        UNEXPECTED_TAG, INVALID_WIRE_TYPE, SIZE_LIMIT, NEGATIVE_SUBMESSAGE_SIZE, MALFORMED_VARIANT, ZERO_TAG
    }

    private static final Map<String, MagickDelimitedProtobufReader.EXCEPTION_CODE> exceptionMsgList;
    static {
        exceptionMsgList = new HashMap<>();
        exceptionMsgList.put(
                "Message was missing required fields.  (Lite runtime could not determine which fields were missing).",
                EXCEPTION_CODE.MISSING_REQ_FIELDS);
        exceptionMsgList.put(
                "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.",
                EXCEPTION_CODE.INPUT_ENDED_UNEXPECTLY);
        exceptionMsgList.put(
                "Protocol message end-group tag did not match expected tag.",
                EXCEPTION_CODE.UNEXPECTED_TAG);
        exceptionMsgList.put(
                "Protocol message tag had invalid wire type.",
                EXCEPTION_CODE.INVALID_WIRE_TYPE);
        exceptionMsgList.put(
                "Protocol message contained an invalid tag (zero).",
                EXCEPTION_CODE.ZERO_TAG);
        exceptionMsgList.put(
                "Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.",
                EXCEPTION_CODE.SIZE_LIMIT);
        exceptionMsgList.put(
                "CodedInputStream encountered an embedded string or message which claimed to have negative size.",
                EXCEPTION_CODE.NEGATIVE_SUBMESSAGE_SIZE);
        exceptionMsgList.put(
                "CodedInputStream encountered a malformed varint.",
                EXCEPTION_CODE.MALFORMED_VARIANT);
    }

    private final InputStream inputStream;
    private final SizeReaderSafeData dataRecycler = new SizeReaderSafeData();

    private class SizeReaderSafeData {
        private CircularFifoQueue<Byte> data = new CircularFifoQueue<>(Long.SIZE / 8 + 1);
        private boolean recycling = false;
        private InputStream override_stream = null;

        void recycleBuffer(byte[] buf) {
            recycleBuffer(buf, 0, buf.length);
        }

        void recycleBuffer(byte[] buf, int offset, int length) {
            data.clear();
            override_stream = new ByteArrayInputStream(buf, offset, length);
        }

        private long readRawVariant64SlowPath(InputStream is) throws IOException, EndOfStreamException {
            long result = 0L;

            for (int shift = 0; shift < 64; shift += 7) {
                int b = is.read();
                result |= (long) (b & 127) << shift;
                if ((b & 128) == 0) {
                    return result;
                }
            }

            throw new InvalidProtocolBufferException(
                    "CodedInputStream encountered a malformed variant.");
        }

        private byte[] save() {
            byte[] currently_saved = new byte[data.size()];
            for (int i = 0; !data.isEmpty(); ++i) {
                currently_saved[i] = data.remove();
            }
            return currently_saved;
        }

        private SizeReaderSafeData put(byte[] bytes) {
            for (byte b : bytes)
                data.add(b);
            return this;
        }

        long readSize() throws IOException, EndOfStreamException {
            try {
                return readRawVariant64SlowPath(new InputStream() {
                    @Override
                    public int read() throws IOException {
                        try {
                            int v = SizeReaderSafeData.this.read();
                            if (recycling)
                                data.add((byte) v);
                            return v;
                        } catch (EndOfStreamException e) {
                            return -1;
                        }
                    }
                });
            } catch (InvalidProtocolBufferException ex) {
                return -1;
            }
            /*
            try {
                if (recycling) {
                    final byte[] save_before = save();
                    final ByteArrayInputStream before_stream = new ByteArrayInputStream(save_before);

                    long res = readRawVariant64SlowPath(new InputStream() {
                        @Override
                        public int read() throws IOException {
                            int r = before_stream.read();
                            if (r == -1)
                                try {
                                    return SizeReaderSafeData.this.read();
                                } catch (EndOfStreamException e) {
                                    e.printStackTrace();
                                    return -1;
                                }
                            return r;
                        }
                    });

                    byte[] save_after = save();

                    put(save_before).put(save_after);

                    return res;
                } else
                    return readRawVariant64SlowPath(new InputStream() {
                        @Override
                        public int read() throws IOException {
                            try {
                                return SizeReaderSafeData.this.read();
                            } catch (EndOfStreamException e) {
                                e.printStackTrace();
                                return -1;
                            }
                        }
                    });
            } catch (InvalidProtocolBufferException ex) {
                return -1;
            }*/
        }

        void reset() {
            data.clear();
        }

        int read() throws IOException, EndOfStreamException {
            if (recycling) {
                if (!data.isEmpty())
                    return data.remove();
                else
                    recycling = false;
            }

            if (override_stream != null) {
                int res = override_stream.read();
                if (res == -1)
                    override_stream = null;
                else {
                    data.add((byte) res);
                    return res;
                }
            }

            int res = inputStream.read();
            if (res == -1)
                throw new EndOfStreamException("Input stream was closed");
            data.add((byte) res);
            return res;
        }

        void recycleAll() {
            recycling = true;
        }

        SizeReaderSafeData shift(int N) {
            if (!data.isEmpty())
                data.remove();
            return this;
        }

        int readNoSave() throws IOException, EndOfStreamException {
            if (recycling) {
                if (!data.isEmpty())
                    return data.remove();
                else
                    recycling = false;
            }
            int res = -1;
            try {
                res = inputStream.read();
            } catch (IOException ex) { /* Обчно означает, что поток закрыт */ }
            if (res == -1)
                throw new EndOfStreamException("Input stream was closed");
            return res;
        }

        int readAllSaved(byte[] buf, int offset) {
            int i = 0;
            if (recycling) {
                while (!data.isEmpty())
                    buf[offset + i++] = data.remove();
                recycling = false;
            }
            return i;
        }

        int read(byte[] buf) throws IOException {
            return read(buf, 0, buf.length);
        }

        int read(byte[] buf, int offset, int len) throws IOException {
            if (buf.length == 0) {
                if (len != 0)
                    throw new ArrayIndexOutOfBoundsException(
                            String.format("Buffer size == 0, but required read %d", len));
                return 0;
            }
            int read_saved = readAllSaved(buf, offset);
            int read_override = 0;
            if (override_stream != null) {
                read_override = override_stream.read(buf, offset + read_saved, len - read_saved);
            }
            return read_saved + read_override +
                    inputStream.read(buf, offset + read_saved + read_override,
                            len - read_saved - read_override);
        }

        public SizeReaderSafeData stopRecycling() {
            recycling = false;
            return this;
        }
    }

    public class EndOfStreamException extends Exception {
        public EndOfStreamException() {
            super();
        }

        public EndOfStreamException(String reason) {
            super(reason);
        }
    }

    public MagickDelimitedProtobufReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public <T extends GeneratedMessageLite> T read(Class<T> messageType)
            throws IOException, EndOfStreamException, InterruptedException,
            ReflectiveOperationException {

        STATE state = STATE.RECEIVING_SIZE;
        byte[] buf = null;
        byte messageCorrectHeader = getMessageFirstByte(messageType);
        Method parser = messageType.getMethod(PARSE_FROM_METHOD_NAME, CodedInputStream.class);

        dataRecycler.reset();

        while (true) {
            if (Thread.currentThread().isInterrupted())
                throw new InterruptedException("Thread was interrupted");

            switch (state) {
                case RECEIVING_SIZE: {
                    int size = (int) dataRecycler.readSize();
                    if (size < 2 || size > MAX_MSG_SIZE) {
                        dataRecycler.shift(1).recycleAll();
                    } else {
                        dataRecycler.stopRecycling();
                        if (buf == null || size != buf.length)
                            buf = new byte[size];
                        state = STATE.CHECK_MESSAGE_FIRST_TAG;
                    }
                    break;
                }
                case CHECK_MESSAGE_FIRST_TAG:
                    int b = dataRecycler.read();
                    if (b == messageCorrectHeader) {
                        state = STATE.RECEIVING_BODY;
                    } else {
                        dataRecycler.shift(1).recycleAll();
                        state = STATE.RECEIVING_SIZE;
                    }
                    break;
                case RECEIVING_BODY: {
                    int read = dataRecycler.read(buf, 1, buf.length - 1);
                    buf[0] = messageCorrectHeader;
                    try {
                        if (buf.length - 1 > read) {
                            return (T) parser.invoke(null, CodedInputStream.newInstance(buf,
                                    0, read));
                        }
                        if (buf.length - 1 == read) {
                            return (T) parser.invoke(null, CodedInputStream.newInstance(buf));
                        } else {
                            throw new RuntimeException(String.format(
                                    "Read more then required (%d of %d), impossible!",
                                    read, buf.length));
                        }
                    } catch (InvocationTargetException e) {
                        EXCEPTION_CODE code = exceptionMsgList.get(e.getTargetException().getMessage());
                        if (code != null) {
                            dataRecycler.recycleBuffer(buf, 0, read + 1);
                            state = STATE.RECEIVING_SIZE;
                        } else
                            throw e;
                    }
                }
            }
        }
    }

    public byte getMessageFirstByte(Class<? extends GeneratedMessageLite> messageClass)
            throws ReflectiveOperationException {
        // весь этот метод один большой грязный хак
        // Может сломаться при изменение protobuf
        // есть идеи как сделать лучше?

        Method get_default_method = messageClass.getMethod(GET_DEFAULT_INSTANCE);
        Object defaultMsg = get_default_method.invoke(null);
        Field bitField0_ = defaultMsg.getClass().getDeclaredField(BIT_FIELD_0_);
        bitField0_.setAccessible(true);
        bitField0_.set(defaultMsg, 1);
        Method toByteArray_method = defaultMsg.getClass().getMethod(TO_BYTE_ARRAY);
        return ((byte[]) toByteArray_method.invoke(defaultMsg))[0];
    }
}
