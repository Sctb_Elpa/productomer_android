package ru.sctbelpa.productomer_r4_2;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

import static org.junit.Assert.*;

// Эти тесты запускать на железе
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("ru.sctbelpa.productomer_r4_2", appContext.getPackageName());
    }
}
