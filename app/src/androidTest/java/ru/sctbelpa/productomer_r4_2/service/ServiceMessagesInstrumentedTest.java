package ru.sctbelpa.productomer_r4_2.service;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Pair;

import org.jetbrains.annotations.Contract;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import ru.sktbelpa.Productomer_2.ProtobufDevice0000E002;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class ServiceMessagesInstrumentedTest {

    @ClassRule
    public static final ServiceTestRule mServiceRule = new ServiceTestRule();
    private static ProductomerService service;

    @BeforeClass
    public static void BeforeClass() throws TimeoutException {
        // Create the service Intent.
        Intent serviceIntent = new Intent(InstrumentationRegistry.getTargetContext(),
                ProductomerService.class);
        IBinder binder = mServiceRule.bindService(serviceIntent);

        service = ((ProductomerService.LocalBinder) binder).getService();

        BluetoothDevice TestDevice = TestUtils.findTestDevise(
                service.getAdapter().getBondedDevices(), TestUtils.testMAC);
        service.setTargetServer(TestDevice, ProductomerService.defaultUUID);
    }

    @AfterClass
    public static void AfterClass() throws InterruptedException {
        service.dispatcher.interrupt();
        service.dispatcher.join();
    }


    @Contract(pure = true)
    @NonNull
    private IFailCallback getErrorCallback(final Object event) {
        return new IFailCallback() {
            @Override
            public void processError(Exception ex) {
                synchronized (event) {
                    event.notify();
                }
            }
        };
    }

    @Contract(pure = true)
    @NonNull
    private ISuccessCallback getSuccessCallback(final Object event) {
        return new ISuccessCallback() {
            @Override
            public void processResult(ProtobufDevice0000E002.Response response) {
                assertEquals(ProtobufDevice0000E002.INFO.PRODUCTOMER_2_ID_VALUE,
                        response.getDeviceID());
                assertEquals(ProtobufDevice0000E002.STATUS.OK, response.getGlobalStatus());
                assertEquals(ProtobufDevice0000E002.INFO.PROTOCOL_VERSION_VALUE,
                        response.getProtocolVersion());
                synchronized (event) {
                    event.notify();
                }
            }
        };
    }

    @Test
    public void testPing() throws Exception {
        final Object event = new Object();

        ProductomerService.CommandBuilder commandBuilder = service.newCommandBuilder();

        commandBuilder.successCallback = spy(getSuccessCallback(event));
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand ping_cmd = commandBuilder.build(
                ProtobufDevice0000E002.INFO.PRODUCTOMER_2_ID_VALUE);

        ping_cmd.setTimeout(100).execute();

        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(Exception.class));
        verify(commandBuilder.successCallback).processResult(
                any(ProtobufDevice0000E002.Response.class));
    }

    @Test
    public void testScan() throws Exception {
        final Object event = new Object();

        ProductomerService.CommandBuilder commandBuilder = service.newCommandBuilder();

        commandBuilder.successCallback = spy(getSuccessCallback(event));
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand ping_cmd = commandBuilder.buildBroadcast();

        ping_cmd.setTimeout(100).execute();
        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(Exception.class));
        verify(commandBuilder.successCallback).processResult(
                any(ProtobufDevice0000E002.Response.class));
    }

    @Test
    public void testNormalSetSettings() throws Exception {
        final Object event = new Object();

        final ProtobufDevice0000E002.WriteSettingsRequest.Builder writeSettingsBuilder =
                ProtobufDevice0000E002.WriteSettingsRequest.newBuilder()
                        .setPartNumber(42)
                        .setMeasureTimeT1(1000)
                        .setMeasureTimeT2(995)
                        .setReferenceFrequency(12000005)
                        .setEnableT1Chanel(false)
                        .setEnableT2Chanel(false)
                        .setShowT1Freq(false)
                        .setShowT2Freq(false)
                        .setT1Coeffs(ProtobufDevice0000E002.TCoeffs.newBuilder()
                                .setT0(195).setF0(312).addAllC(Arrays.asList(1.0F, 2.0F, 3.0F)))
                        .setT2Coeffs(ProtobufDevice0000E002.TCoeffs.newBuilder()
                                .setT0(0.9F).setF0(3.5F).addAllC(Arrays.asList(-.3F, 8.7F, -1.0F)));

        final ProductomerService.CommandBuilder commandBuilder = service.newCommandBuilder()
                .settingsOperation(writeSettingsBuilder);

        commandBuilder.successCallback = spy(new ISuccessCallback() {
            @Override
            public void processResult(ProtobufDevice0000E002.Response response) {
                assertTrue(response.hasSettings());
                assertEquals(ProtobufDevice0000E002.STATUS.OK, response.getGlobalStatus());

                ProtobufDevice0000E002.SettingsResponse settings = response.getSettings();

                assertEquals(writeSettingsBuilder.getPartNumber(), settings.getPartNumber());
                assertEquals(writeSettingsBuilder.getMeasureTimeT1(), settings.getMeasureTimeT1());
                assertEquals(writeSettingsBuilder.getMeasureTimeT2(), settings.getMeasureTimeT2());
                assertEquals(writeSettingsBuilder.getReferenceFrequency(), settings.getReferenceFrequency());
                assertEquals(writeSettingsBuilder.getEnableT1Chanel(), settings.getEnableT1Chanel());
                assertEquals(writeSettingsBuilder.getEnableT2Chanel(), settings.getEnableT2Chanel());
                assertEquals(writeSettingsBuilder.getShowT1Freq(), settings.getShowT1Freq());
                assertEquals(writeSettingsBuilder.getShowT2Freq(), settings.getShowT2Freq());
                assertTrue(writeSettingsBuilder.getT1Coeffs().equals(settings.getT1Coeffs()));
                assertTrue(writeSettingsBuilder.getT2Coeffs().equals(settings.getT2Coeffs()));

                synchronized (event) {
                    event.notify();
                }
            }
        });
        commandBuilder.failCallback = spy(getErrorCallback(event));

        IProductomerCommand cmd = commandBuilder.buildBroadcast();

        cmd.setTimeout(100).execute();
        synchronized (event) {
            event.wait();
        }

        verify(commandBuilder.failCallback, never()).processError(any(Exception.class));
        verify(commandBuilder.successCallback).processResult(
                any(ProtobufDevice0000E002.Response.class));
    }

    abstract class settingsRequestBuilder {
        abstract ProtobufDevice0000E002.WriteSettingsRequest.Builder build();

        ProtobufDevice0000E002.WriteSettingsRequest.Builder newBuilder() {
            return ProtobufDevice0000E002.WriteSettingsRequest.newBuilder();
        }
    }

    private Pair<settingsRequestBuilder, ISuccessCallback>
    createBuilderVerificator(final String field, final Object value, Class arg_class,
                             final boolean isCorrect) {
        final Method setter, getter;
        try {
            setter = ProtobufDevice0000E002.WriteSettingsRequest.Builder.class.getMethod(
                    "set" + field, arg_class);
            getter = ProtobufDevice0000E002.SettingsResponse.class.getMethod("get" + field);
        } catch (NoSuchMethodException e) {
            return null;
        }

        return new Pair<settingsRequestBuilder, ISuccessCallback>(new settingsRequestBuilder() {
            @Override
            public ProtobufDevice0000E002.WriteSettingsRequest.Builder build() {
                try {
                    return (ProtobufDevice0000E002.WriteSettingsRequest.Builder) setter.invoke(
                            newBuilder(), value);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }, new ISuccessCallback() {
            @Override
            public void processResult(ProtobufDevice0000E002.Response response) {
                ProtobufDevice0000E002.SettingsResponse settings = response.getSettings();
                try {
                    if (isCorrect)
                        assertTrue(field, value.equals(getter.invoke(settings)));
                    else
                        assertFalse(field, value.equals(getter.invoke(settings)));
                } catch (Exception ex) {
                    assertNotNull(null);
                }
            }
        });
    }

    @Test
    public void testIncorrectSettings() throws Exception {
        final Object event = new Object();
        IFailCallback errCallback = spy(getErrorCallback(event));
        final String T1Coeffs = "T1Coeffs";
        final String T2Coeffs = "T2Coeffs";

        List<Pair<settingsRequestBuilder, ISuccessCallback>> tests = Arrays.asList(
                createBuilderVerificator("MeasureTimeT1", 0, int.class, false),
                createBuilderVerificator("MeasureTimeT1", 100, int.class, true),
                createBuilderVerificator("MeasureTimeT1", 2500, int.class, false),

                createBuilderVerificator("MeasureTimeT2", 0, int.class, false),
                createBuilderVerificator("MeasureTimeT2", 100, int.class, true),
                createBuilderVerificator("MeasureTimeT2", 2500, int.class, false),

                createBuilderVerificator("ReferenceFrequency", -1, int.class, false),
                createBuilderVerificator("ReferenceFrequency", 12000499, int.class, true),
                createBuilderVerificator("ReferenceFrequency", Integer.MAX_VALUE, int.class, false),

                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(Float.NaN).setT0(0).addAllC(
                                Arrays.asList(0F, 0F, 0F)),
                        ProtobufDevice0000E002.TCoeffs.Builder.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(Float.NaN).addAllC(
                                Arrays.asList(0F, 0F, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(Float.NaN, 0F, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(0F, Float.NaN, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(0F, 0F, Float.NaN)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T1Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(1F, 0F, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, true),

                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(Float.NaN).setT0(0).addAllC(
                                Arrays.asList(0F, 0F, 0F)),
                        ProtobufDevice0000E002.TCoeffs.Builder.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(Float.NaN).addAllC(
                                Arrays.asList(0F, 0F, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(Float.NaN, 0F, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(0F, Float.NaN, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(0F, 0F, Float.NaN)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, false),
                createBuilderVerificator(T2Coeffs,
                        ProtobufDevice0000E002.TCoeffs.newBuilder().setF0(0).setT0(0).addAllC(
                                Arrays.asList(1F, 0F, 0F)).build(),
                        ProtobufDevice0000E002.TCoeffs.class, true),

                new Pair<settingsRequestBuilder, ISuccessCallback>(new settingsRequestBuilder() {
                    @Override
                    public ProtobufDevice0000E002.WriteSettingsRequest.Builder build() {
                        return newBuilder().setMeasureTimeT1(2500);
                    }
                }, new ISuccessCallback() {
                    @Override
                    public void processResult(ProtobufDevice0000E002.Response response) {
                        assertTrue(response.getSettings().getMeasureTimeT1() != 2500);

                        synchronized (event) {
                            event.notify();
                        }
                    }
                })
        );

        for (Pair<settingsRequestBuilder, ISuccessCallback> test : tests) {
            ProductomerService.CommandBuilder builder =
                    service.newCommandBuilder().settingsOperation(test.first.build());
            builder.successCallback = test.second;
            builder.failCallback = errCallback;
            // Получается так, что сообщения отправляются одной пачкой тоесть на стороне приймника
            // нужен полный разбор.
            builder.build(ProtobufDevice0000E002.INFO.PRODUCTOMER_2_ID_VALUE)
                    .setTimeout(500).execute();
        }

        synchronized (event) {
            event.wait();
        }

        verify(errCallback, never()).processError(any(Exception.class));
    }
}
