package ru.sctbelpa.productomer_r4_2.service;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.Nullable;

import java.util.UUID;

class TestUtils {
    static final String testMAC = "00:1A:7D:DA:71:13";
    static final UUID invalidUUID = UUID.fromString("00000000-0000-0000-0000-000000000000");

    @Nullable
    static BluetoothDevice findTestDevise(Iterable<BluetoothDevice> devices,
                                          String address) {
        for (BluetoothDevice d : devices) {
            if (d.getAddress().equals(address))
                return d;
        }
        return null;
    }
}
